package com.computerstore;

import com.computerstore.model.User;
import com.computerstore.model.security.Role;
import com.computerstore.model.security.UserRole;
import com.computerstore.repository.ProductRepository;
import com.computerstore.repository.TestDataPopulator;
import com.computerstore.service.UserService;
import com.computerstore.utility.SecurityUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;


//drop tables from second run
@SpringBootApplication
public class ComputerstoreApplication implements CommandLineRunner {

    @Autowired
    private TestDataPopulator testDataPopulator;

    public static void main(String[] args) {
        SpringApplication.run(ComputerstoreApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        testDataPopulator.createAdmin();
        testDataPopulator.createProducts();

    }
}