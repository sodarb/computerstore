package com.computerstore.service;

import org.springframework.web.multipart.MultipartFile;

public interface ImageService {
    void uploadImage(MultipartFile productImage, Long id) throws Exception;

    void replaceImage(MultipartFile productImage, Long id) throws Exception;
}
