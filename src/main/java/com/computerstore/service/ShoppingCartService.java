package com.computerstore.service;

import com.computerstore.model.ShoppingCart;

import java.math.BigDecimal;

public interface ShoppingCartService {
    void clearCart(ShoppingCart shoppingCart);

    void updatePrice(ShoppingCart shoppingCart, BigDecimal price);
}
