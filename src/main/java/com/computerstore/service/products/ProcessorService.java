package com.computerstore.service.products;

import com.computerstore.model.products.Processor;

import java.util.List;

public interface ProcessorService {
    List<Processor> findAll();

    List<Processor> findAllActive();
}

