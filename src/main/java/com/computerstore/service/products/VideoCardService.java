package com.computerstore.service.products;

import com.computerstore.model.products.VideoCard;

import java.util.List;

public interface VideoCardService {
    List<VideoCard> findAll();

    List<VideoCard> findAllActive();
}
