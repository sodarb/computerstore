package com.computerstore.service.products;

import com.computerstore.model.products.Motherboard;

import java.util.List;

public interface MotherboardService {
    List<Motherboard> findAll();

    List<Motherboard> findAllActive();
}

