package com.computerstore.service.products;

import com.computerstore.model.products.Product;

import java.util.List;

public interface ProductService {
    Product save(Product product);

    List<Product> findAll();

    List<Product> findAllActive();

    Product findById(Long id);

    List<Product> findAllFeatured();

    List<Product> selectRandomFeaturedProducts(int numSelected);
}