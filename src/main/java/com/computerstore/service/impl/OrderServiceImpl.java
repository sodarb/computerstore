package com.computerstore.service.impl;

import com.computerstore.model.*;
import com.computerstore.model.products.Product;
import com.computerstore.repository.OrderRepository;
import com.computerstore.service.CartItemService;
import com.computerstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CartItemService cartItemService;

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Order createOrder(ShoppingCart shoppingCart, ShippingAddress shippingAddress, BillingAddress billingAddress, User user) {
        Order order = new Order();
        order.setOrderStatus("created");
        order.setShippingAddress(shippingAddress);
        order.setBillingAddress(billingAddress);

        List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);

        for(CartItem cartItem : cartItemList){
            Product product = cartItem.getProduct();
            cartItem.setOrder(order);
            product.setStock(product.getStock()-cartItem.getQty());
            if(product.getStock() == 0){
                product.setActive(false);
                product.setFeatured(false);
            }
        }

        order.setCartItemList(cartItemList);
        order.setTotalPrice(shoppingCart.getFinalSum());
        order.setOrderDate(Calendar.getInstance().getTime());
        shippingAddress.setOrder(order);
        billingAddress.setOrder(order);
        order.setUser(user);

        return orderRepository.save(order);
    }

    @Override
    public List<Order> findAll() {
        return (List<Order>) orderRepository.findAll();
    }
}
