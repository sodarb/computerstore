package com.computerstore.service.impl;

import com.computerstore.service.ImageService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class ImageServiceImpl implements ImageService {

    @Override
    public void uploadImage(MultipartFile productImage, Long id) throws Exception {
        byte[] bytes = productImage.getBytes();
        String name= id+".png";
        BufferedOutputStream stream = new BufferedOutputStream(
                new FileOutputStream(new File("src/main/resources/static/image/product/"+name)));
        stream.write(bytes);
        stream.close();
    }

    @Override
    public void replaceImage(MultipartFile productImage, Long id) throws Exception {
        byte[] bytes = productImage.getBytes();
        String name = id + ".png";

        Files.delete(Paths.get("src/main/resources/static/image/product/"+name));

        BufferedOutputStream stream = new BufferedOutputStream(
                new FileOutputStream(new File("src/main/resource/static/image/product/+name")));
        stream.write(bytes);
        stream.close();
    }
}