package com.computerstore.service.impl;

import com.computerstore.model.CartItem;
import com.computerstore.model.ShoppingCart;
import com.computerstore.model.User;
import com.computerstore.model.products.Product;
import com.computerstore.repository.CartItemRepository;
import com.computerstore.service.CartItemService;
import com.computerstore.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CartItemServiceImpl implements CartItemService {

    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private ShoppingCartService shoppingCartService;


    @Override
    public CartItem save(CartItem cartItem) {
        return cartItemRepository.save(cartItem);
    }

    @Override
    public CartItem addToShoppingCart(Product product, User user, int qty) {
        ShoppingCart shoppingCart = user.getShoppingCart();
        List<CartItem> cartItemList = findByShoppingCart(shoppingCart);

        for(CartItem cartItem : cartItemList){
            if(cartItem.getProduct().getId().equals(product.getId())){
                shoppingCartService.updatePrice(shoppingCart,cartItem.getSum().negate());
                cartItem.setQty(cartItem.getQty()+qty);
                cartItem.setSum(new BigDecimal(cartItem.getQty()).multiply(new BigDecimal(product.getOurPrice())));
                shoppingCartService.updatePrice(shoppingCart,cartItem.getSum());
                cartItemRepository.save(cartItem);
                return cartItem;
            }
        }

        CartItem cartItem = new CartItem();
        cartItem.setShoppingCart(user.getShoppingCart());
        cartItem.setProduct(product);
        cartItem.setQty(qty);
        cartItem.setSum(new BigDecimal(qty).multiply(new BigDecimal(product.getOurPrice())));
        shoppingCartService.updatePrice(shoppingCart,cartItem.getSum());
        return cartItemRepository.save(cartItem);
    }

    @Override
    public List<CartItem> findByShoppingCart(ShoppingCart shoppingCart) {
        return cartItemRepository.findByShoppingCart(shoppingCart);
    }

    @Override
    public void remove(Long id) {
        cartItemRepository.delete(id);
    }

    @Override
    public CartItem findById(Long id) {
        return cartItemRepository.findOne(id);
    }

    @Override
    public void updateQty(CartItem cartItem, int newQty) {
        BigDecimal newSum = new BigDecimal(cartItem.getProduct().getOurPrice()).multiply(new BigDecimal(newQty));
        cartItem.setQty(newQty);
        cartItem.setSum(newSum);
        cartItemRepository.save(cartItem);
    }
}
