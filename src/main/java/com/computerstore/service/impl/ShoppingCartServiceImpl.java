package com.computerstore.service.impl;

import com.computerstore.model.CartItem;
import com.computerstore.model.ShoppingCart;
import com.computerstore.repository.ShoppingCartRepository;
import com.computerstore.service.CartItemService;
import com.computerstore.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private CartItemService cartItemService;

    @Override
    public void clearCart(ShoppingCart shoppingCart) {

        List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);

        for (CartItem cartItem : cartItemList) {
            cartItem.setShoppingCart(null);
            cartItemService.save(cartItem);
        }

        shoppingCart.setFinalSum(new BigDecimal(0));
        shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public void updatePrice(ShoppingCart shoppingCart, BigDecimal price) {
        shoppingCart.setFinalSum(shoppingCart.getFinalSum().add(price));
        shoppingCartRepository.save(shoppingCart);
    }
}
