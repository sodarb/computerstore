package com.computerstore.service.impl;

import com.computerstore.model.Order;
import com.computerstore.model.User;
import com.computerstore.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.InternetAddress;
import java.util.Locale;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Environment env;

    @Autowired
    private TemplateEngine templateEngine;

    @Override
    public MimeMessagePreparator constructOrderConfirmationEmail(User user, Order order, Locale locale) {
        Context context = new Context();
        context.setVariable("order",order);
        context.setVariable("user",user);
        context.setVariable("cartItemList",order.getCartItemList());
        String text=templateEngine.process("orderConfirmationEmailTemplate",context);

        return mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(user.getEmail());
            messageHelper.setSubject("Order Confirmation - "+order.getId());
            messageHelper.setText(text,true);
            messageHelper.setFrom(new InternetAddress("pcpartsstore14@gmail.com"));
        };

    }

    @Override
    public void sendOrderConfirmationEmail(User user, Order order, Locale locale) {
        mailSender.send(constructOrderConfirmationEmail(user,order,locale));
    }

    @Override
    public SimpleMailMessage constructPasswordResetEmail(User user, String password) {
        String message = "Your password is:\n"+password+"\n\nPlease change it next time you log in!";
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject("Computer Store - Reset Password");
        email.setText(message);
        email.setFrom(env.getProperty("support.email"));

        return email;
    }

    @Override
    public void sendPasswordResetEmail(SimpleMailMessage mailMessage) {
        mailSender.send(mailMessage);
    }
}
