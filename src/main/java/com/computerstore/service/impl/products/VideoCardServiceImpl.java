package com.computerstore.service.impl.products;

import com.computerstore.model.products.VideoCard;
import com.computerstore.repository.VideoCardRepository;
import com.computerstore.service.products.VideoCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoCardServiceImpl implements VideoCardService {

    @Autowired
    private VideoCardRepository videoCardRepository;

    @Override
    public List<VideoCard> findAll() {
        return (List<VideoCard>) videoCardRepository.findAll();
    }

    @Override
    public List<VideoCard> findAllActive() {
        return videoCardRepository.findAllByActiveIsTrue();
    }
}
