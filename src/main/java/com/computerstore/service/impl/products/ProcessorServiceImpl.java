package com.computerstore.service.impl.products;

import com.computerstore.model.products.Processor;
import com.computerstore.repository.ProcessorRepository;
import com.computerstore.service.products.ProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcessorServiceImpl implements ProcessorService {

    @Autowired
    private ProcessorRepository repository;

    @Override
    public List<Processor> findAll() {
        return (List<Processor>) repository.findAll();
    }

    @Override
    public List<Processor> findAllActive() {
        return repository.findAllByActiveIsTrue();
    }
}
