package com.computerstore.service.impl.products;

import com.computerstore.model.products.Product;
import com.computerstore.repository.ProductRepository;
import com.computerstore.service.products.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class ProductServiceImpl implements ProductService {


    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public List<Product> findAll() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public Product findById(Long id) {
        return productRepository.findOne(id);
    }

    @Override
    public List<Product> findAllActive() {
        return productRepository.findAllByActiveIsTrue();
    }

    @Override
    public List<Product> findAllFeatured() {
        return productRepository.findAllByFeaturedIsTrue();
    }


    /**
     * Randomly selects a given number of products.
     *
     *
     * @param numSelected the number of selected products
     * @return the list of the selected products
     */
    @Override
    public List<Product> selectRandomFeaturedProducts(int numSelected) {
        List<Product> featuredProducts = findAllFeatured();
        List<Product> selectedProducts = new ArrayList<>();
        int num = numSelected;
        Random random = new Random();
        if(featuredProducts.size() < numSelected){
            num = featuredProducts.size();
        }
        while (selectedProducts.size() < num){
            int index = random.nextInt(featuredProducts.size());
            selectedProducts.add(featuredProducts.get(index));
            featuredProducts.remove(index);
        }

        return selectedProducts;
    }
}
