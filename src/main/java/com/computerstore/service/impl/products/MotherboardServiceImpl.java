package com.computerstore.service.impl.products;

import com.computerstore.model.products.Motherboard;
import com.computerstore.repository.MotherboardRepository;
import com.computerstore.service.products.MotherboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MotherboardServiceImpl implements MotherboardService {

    @Autowired
    private MotherboardRepository motherboardRepository;

    @Override
    public List<Motherboard> findAll() {
        return (List<Motherboard>) motherboardRepository.findAll();
    }

    @Override
    public List<Motherboard> findAllActive() {
        return motherboardRepository.findAllByActiveIsTrue();
    }
}
