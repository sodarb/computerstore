package com.computerstore.service.impl;

import com.computerstore.model.ShoppingCart;
import com.computerstore.model.User;
import com.computerstore.model.security.UserRole;
import com.computerstore.repository.RoleRepository;
import com.computerstore.repository.UserRepository;
import com.computerstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User createUser(User user, Set<UserRole> userRoles) throws Exception {
        User tempUser = userRepository.findByUsername(user.getUsername());

        if (tempUser != null) {
            throw new Exception("User already exists!");
        } else {
            for (UserRole ur : userRoles) {
                roleRepository.save(ur.getRole());
            }

            user.getUserRoles().addAll(userRoles);

            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.setUser(user);
            user.setShoppingCart(shoppingCart);

            tempUser = userRepository.save(user);
            System.out.println("creating user");
        }

        return tempUser;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }
}
