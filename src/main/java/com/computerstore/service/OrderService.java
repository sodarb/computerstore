package com.computerstore.service;

import com.computerstore.model.*;

import java.util.List;

public interface OrderService {

    Order createOrder(
            ShoppingCart shoppingCart,
            ShippingAddress shippingAddress,
            BillingAddress billingAddress,
            User user);

    Order save(Order order);

    List<Order> findAll();
}
