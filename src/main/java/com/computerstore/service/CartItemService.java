package com.computerstore.service;

import com.computerstore.model.CartItem;
import com.computerstore.model.ShoppingCart;
import com.computerstore.model.User;
import com.computerstore.model.products.Product;

import java.util.List;

public interface CartItemService {

    CartItem save(CartItem cartItem);

    CartItem addToShoppingCart(Product product, User user, int qty);

    List<CartItem> findByShoppingCart(ShoppingCart shoppingCart);

    CartItem findById(Long id);

    void updateQty(CartItem cartItem, int newQty);

    void remove(Long id);
}
