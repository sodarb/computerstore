package com.computerstore.service;

import com.computerstore.model.Order;
import com.computerstore.model.User;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.util.Locale;

public interface MailService {
    MimeMessagePreparator constructOrderConfirmationEmail(User user, Order order, Locale locale);

    void sendOrderConfirmationEmail(User user, Order order, Locale locale);

    void sendPasswordResetEmail(SimpleMailMessage mailMessage);

    SimpleMailMessage constructPasswordResetEmail(User user, String password);
}

