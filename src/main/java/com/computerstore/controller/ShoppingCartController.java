package com.computerstore.controller;

import com.computerstore.model.CartItem;
import com.computerstore.model.ShoppingCart;
import com.computerstore.model.User;
import com.computerstore.model.products.Product;
import com.computerstore.service.CartItemService;
import com.computerstore.service.ShoppingCartService;
import com.computerstore.service.UserService;
import com.computerstore.service.products.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
public class ShoppingCartController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartItemService cartItemService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @GetMapping("/shoppingCart")
    public String shoppingCart(
            Principal principal,
            Model model) {
        User user = userService.findByUsername(principal.getName());
        ShoppingCart shoppingCart = user.getShoppingCart();
        List<CartItem> cartItems = shoppingCart.getCartItemList();
        if (cartItems.size() == 0) {
            model.addAttribute("emptyCart", true);
        }
        model.addAttribute("cartItemList", cartItems);
        model.addAttribute("price", shoppingCart.getFinalSum());

        return "shoppingCart";
    }

    @PostMapping("/product/addToCart")
    public String addToCart(
            @ModelAttribute("id") String id,
            Principal principal,
            Model model
    ) {
        User user = userService.findByUsername(principal.getName());
        Product product = productService.findById(Long.parseLong(id));

        cartItemService.addToShoppingCart(product, user, 1);
        model.addAttribute("addedToCart", true);
        return "redirect:/shoppingCart";
    }

    @PostMapping("/shoppingCart/updateCart")
    public String updateCart(@RequestParam(value = "action", required = true) String action,
                             @ModelAttribute("id") Long id,
                             @ModelAttribute("qty") int qty,
                             Model model) {
        CartItem cartItem = cartItemService.findById(id);
        shoppingCartService.updatePrice(cartItem.getShoppingCart(),cartItem.getSum().negate());

        if (action.equals("remove")) {
            cartItemService.remove(id);
        } else if (action.equals("update")) {
            if(cartItem.getProduct().getStock() < qty){
                model.addAttribute("notEnoughStock",true);
            }

            cartItemService.updateQty(cartItem,qty);
            shoppingCartService.updatePrice(cartItem.getShoppingCart(),cartItem.getSum());

        }

        return "redirect:/shoppingCart";
    }

}
