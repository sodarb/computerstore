package com.computerstore.controller;

import com.computerstore.model.products.Product;
import com.computerstore.service.products.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public String index(Model model) {
        List<Product> featuredProducts = productService.selectRandomFeaturedProducts(4);
        model.addAttribute("featuredProductList", featuredProducts);
        return "index";
    }


}
