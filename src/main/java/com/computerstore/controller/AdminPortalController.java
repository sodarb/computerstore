package com.computerstore.controller;

import com.computerstore.model.Order;
import com.computerstore.model.products.Motherboard;
import com.computerstore.model.products.Processor;
import com.computerstore.model.products.Product;
import com.computerstore.model.products.VideoCard;
import com.computerstore.service.ImageService;
import com.computerstore.service.OrderService;
import com.computerstore.service.products.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminPortalController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private OrderService orderService;

    @GetMapping("")
    public String admin() {

        return "adminHome";
    }


    @GetMapping("/productList")
    public String productList(Model model) {
        List<Product> productList = productService.findAll();
        model.addAttribute("productList", productList);
        return "productList";
    }

    @GetMapping("/addProduct")
    public String addProduct(Model model) {
        model.addAttribute("processor", new Processor());
        model.addAttribute("motherboard", new Motherboard());
        model.addAttribute("videoCard", new VideoCard());
        return "addProduct";
    }

    @PostMapping("/addProduct/processor")
    public String addNewProcessor(@ModelAttribute("processor") Processor processor, Model model) {
        productService.save(processor);

        MultipartFile processorImage = processor.getImage();
        try{
            imageService.uploadImage(processorImage,processor.getId());
        }catch (Exception e){
            model.addAttribute("uploadFailed",true);
            return "/admin/addProduct";
        }
        return "redirect:/admin/productList";
    }

    @PostMapping("/addProduct/motherboard")
    public String addNewMotherboard(@ModelAttribute("motherboard") Motherboard motherboard, Model model) {
        productService.save(motherboard);

        MultipartFile motherboardImage = motherboard.getImage();
        try{
            imageService.uploadImage(motherboardImage,motherboard.getId());
        }catch (Exception e){
            model.addAttribute("uploadFailed",true);
            return "/admin/addProduct";
        }
        return "redirect:/admin/productList";
    }

    @PostMapping("/addProduct/videoCard")
    public String addNewVideoCard(@ModelAttribute("videoCard") VideoCard videoCard, Model model) {
        productService.save(videoCard);

        MultipartFile videoCardImage = videoCard.getImage();
        try {
            imageService.uploadImage(videoCardImage,videoCard.getId());
        }catch (Exception e){
            model.addAttribute("uploadFailed",true);
            return "/admin/addProduct";
        }
        return "redirect:/admin/productList";
    }

    @GetMapping("/productInfo")
    public String viewProduct(@RequestParam("id") Long id, Model model) {
        Product product = productService.findById(id);
        model.addAttribute("product", product);
        return "productInfo";
    }

    @PostMapping("/editProcessor")
    public String editProcessor(@ModelAttribute("product") Processor product, Model model){
        productService.save(product);

        MultipartFile productImage = product.getImage();

        if(!productImage.isEmpty()){
            try{
                imageService.replaceImage(productImage,product.getId());
            }catch (Exception e){
                model.addAttribute("uploadFailed", true);
                return "/admin/productInfo?id="+product.getId();
            }
        }
        return "redirect:/admin/productInfo?id="+product.getId();
    }

    @PostMapping("/editMotherboard")
    public String editMotherboard(@ModelAttribute("product") Motherboard product){
        productService.save(product);
        return "redirect:/admin/productInfo?id="+product.getId();
    }

    @PostMapping("/editVideoCard")
    public String editVideoCard(@ModelAttribute("product") VideoCard product){
        productService.save(product);
        return "redirect:/admin/productInfo?id="+product.getId();
    }

    @PostMapping("/deleteProduct")
    public String deleteProduct(@ModelAttribute("id") String id){
        Product product = productService.findById(Long.parseLong(id));
        if(product.isActive()){
            product.setActive(false);
            product.setFeatured(false);
        }else{
            product.setActive(true);
        }

        productService.save(product);
        return "redirect:/admin/productList";
    }

    @GetMapping("/orderList")
    public String orders(Model model){
        List<Order> orderList = orderService.findAll();
        model.addAttribute("orderList", orderList);
        return "orderList";
    }


}
