package com.computerstore.controller;

import com.computerstore.model.products.Motherboard;
import com.computerstore.model.products.Processor;
import com.computerstore.model.products.Product;
import com.computerstore.model.products.VideoCard;
import com.computerstore.service.products.MotherboardService;
import com.computerstore.service.products.ProcessorService;
import com.computerstore.service.products.ProductService;
import com.computerstore.service.products.VideoCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProcessorService processorService;

    @Autowired
    private MotherboardService motherboardService;

    @Autowired
    private VideoCardService videoCardService;

    @GetMapping("/product")
    public String productDetails(@RequestParam("id") Long id, Model model) {
        Product product = productService.findById(id);
        model.addAttribute("product", product);

        return "productDetail";
    }


    @GetMapping("/products")
    public String products(Model model) {
        List<Product> productList = productService.findAllActive();
        model.addAttribute("productList", productList);
        model.addAttribute("currentCategory","All");
        return "products";
    }

    @GetMapping("/productsByCategory")
    public String productsByCategory(@RequestParam("category") String category, Model model) {
        List<Product> productList = new ArrayList<>();
        switch (category) {

            case "processor":
                List<Processor> processorList = processorService.findAllActive();
                productList.addAll(processorList);
                model.addAttribute("currentCategory","Processors");
                break;
            case "motherboard":
                List<Motherboard> motherboardList = motherboardService.findAllActive();
                productList.addAll(motherboardList);
                model.addAttribute("currentCategory","Motherboards");
                break;
            case "videoCard":
                List<VideoCard> videoCardList = videoCardService.findAllActive();
                productList.addAll(videoCardList);
                model.addAttribute("currentCategory","Video Cards");
                break;
            default:
                productList = productService.findAllActive();
                model.addAttribute("currentCategory","All");
                break;
        }
        model.addAttribute("productList", productList);
        return "products";
    }
}
