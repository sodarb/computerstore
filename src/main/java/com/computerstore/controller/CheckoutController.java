package com.computerstore.controller;

import com.computerstore.model.*;
import com.computerstore.service.MailService;
import com.computerstore.service.OrderService;
import com.computerstore.service.ShoppingCartService;
import com.computerstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;
import java.util.Locale;

@Controller
public class CheckoutController {

    @Autowired
    private UserService userService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private MailService mailService;

    @GetMapping("/checkout")
    public String checkout(
            Principal principal,
            Model model) {
        User user = userService.findByUsername(principal.getName());
        ShoppingCart shoppingCart = user.getShoppingCart();
        model.addAttribute("shoppingCart", shoppingCart);
        model.addAttribute("shippingAddress", new ShippingAddress());
        model.addAttribute("billingAddress", new BillingAddress());
        return "checkout";
    }

    @GetMapping("/orderConfirmationPage")
    public String orderConfirm(){
        return "orderConfirmationPage";
    }

    @PostMapping("/placeOrder")
    public String placeOrder(
            Principal principal,
            @ModelAttribute("shippingAddress") ShippingAddress shippingAddress,
            @ModelAttribute("billingAddress") BillingAddress billingAddress

    ) {
        User user = userService.findByUsername(principal.getName());
        Order order = orderService.createOrder(user.getShoppingCart(),shippingAddress,billingAddress,user);
        orderService.save(order);
        shoppingCartService.clearCart(user.getShoppingCart());
        mailService.sendOrderConfirmationEmail(user,order, Locale.ENGLISH);

        return "redirect:/orderConfirmationPage";
    }
}
