package com.computerstore.controller;


import com.computerstore.model.User;
import com.computerstore.model.security.Role;
import com.computerstore.model.security.UserRole;
import com.computerstore.service.MailService;
import com.computerstore.service.UserService;
import com.computerstore.service.impl.UserSecurityService;
import com.computerstore.utility.SecurityUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

/**
 * Controlling requests associated with users
 */

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @Autowired
    private UserSecurityService userSecurityService;


    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("classActiveLogin",true);
        return "login";
    }

    @PostMapping("/register")
    public String registerNewUser(@ModelAttribute("user") @Valid User user, BindingResult bindingResult, Model model) {
        if (userService.findByUsername(user.getUsername()) != null) {
            model.addAttribute("usernameExists", true);
            model.addAttribute("classActiveRegister",true);
            return "/login";
        }

        if (userService.findByEmail(user.getEmail()) != null) {
            model.addAttribute("emailExists", true);
            model.addAttribute("classActiveRegister",true);
            return "/login";
        }
        if(bindingResult.hasErrors()){
            model.addAttribute("classActiveRegister",true);
            return "/login";
        }
        String encryptedPassword = SecurityUtility.passwordEncoder().encode(user.getPassword());
        user.setPassword(encryptedPassword);

        Role role = new Role();
        role.setRoleId(1);
        role.setName("ROLE_USER");
        Set<UserRole> userRoles = new HashSet<>();
        userRoles.add(new UserRole(user, role));
        try {
            userService.createUser(user, userRoles);
        } catch (Exception e) {
            model.addAttribute("classActiveRegister",true);
            model.addAttribute("creatingUserFailed",true);
            e.printStackTrace();
            return "/login";
        }

        return "redirect:/login";


    }

    @PostMapping("/resetPassword")
    public String resetPassword(
            Model model,
            @ModelAttribute("email") String email
    ) {
        User user = userService.findByEmail(email);

        if (user == null) {
            model.addAttribute("emailNotExists", true);
            model.addAttribute("classActiveResetPassword",true);
            return "login";
        }

        String password = SecurityUtility.randomPassword();
        String encryptedPassword = SecurityUtility.passwordEncoder().encode(password);
        user.setPassword(encryptedPassword);
        userService.save(user);

        SimpleMailMessage mailMessage = mailService.constructPasswordResetEmail(user, password);
        mailService.sendPasswordResetEmail(mailMessage);
        return "redirect:/login";
    }

    @GetMapping("/myProfile")
    public String myProfile(Principal principal, Model model) {
        User user = userService.findByUsername(principal.getName());
        model.addAttribute("user", user);
        return "myProfile";
    }


    @PostMapping("/editUserInfo")
    public String editProfile(
            @ModelAttribute("user") User user,
            @ModelAttribute("currentPassword") String currentPassword,
            @ModelAttribute("newPassword") String newPassword,
            @ModelAttribute("confirmNewPassword") String confirmNewPassword,
            RedirectAttributes redir,
            Model model) {
        User currentUser = userService.findById(user.getId());

        if (!user.getUsername().equals(currentUser.getUsername()) && userService.findByUsername(user.getUsername()) != null) {
            model.addAttribute("userNameExists", true);
            return "myProfile";

        }

        if (!user.getEmail().equals(currentUser.getEmail()) && userService.findByEmail(user.getEmail()) != null) {
            model.addAttribute("emailExists", true);
            return "myProfile";
        }

        if (currentPassword != null && !newPassword.isEmpty()) {
            if (!newPassword.equals(confirmNewPassword)) {
                model.addAttribute("confirmPwNotMatching", true);
                return "myProfile";
            }
            BCryptPasswordEncoder pwEncoder = SecurityUtility.passwordEncoder();
            String dbPassword = currentUser.getPassword();
            if (pwEncoder.matches(user.getPassword(), dbPassword)) {
                currentUser.setPassword(pwEncoder.encode(newPassword));
            } else {
                model.addAttribute("incorrectPassword", true);
                return "myProfile";
            }
        }

        currentUser.setUsername(user.getUsername());
        currentUser.setEmail(user.getEmail());
        currentUser.setPhoneNumber(user.getPhoneNumber());
        currentUser.setFirstName(user.getFirstName());
        currentUser.setLastName(user.getLastName());

        userService.save(currentUser);

        redir.addAttribute("updateSuccessful", true);
        redir.addAttribute("user", currentUser);


        //authenticating user with new username and password
        UserDetails userDetails = userSecurityService.loadUserByUsername(currentUser.getUsername());

        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return "redirect:/myProfile";


    }




}

