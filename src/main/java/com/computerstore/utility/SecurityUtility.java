package com.computerstore.utility;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.SecureRandom;
import java.util.Random;

public class SecurityUtility {
    private static final String SALT = "salt";

    @Bean
    public static String randomPassword(){
        String chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
        StringBuilder randomPw = new StringBuilder();
        Random random = new Random();

        while(randomPw.length()<18){
            int index = random.nextInt(62);
            randomPw.append(chars.charAt(index));
        }

        return randomPw.toString();
    }

    @Bean
    public static BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder(12,new SecureRandom(SALT.getBytes()));
    }
}
