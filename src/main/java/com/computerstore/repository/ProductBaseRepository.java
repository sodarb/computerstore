package com.computerstore.repository;

import com.computerstore.model.products.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface ProductBaseRepository<T extends Product> extends CrudRepository<T, Long> {

    List<T> findAllByActiveIsTrue();
}