package com.computerstore.repository;

import com.computerstore.model.products.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findAllByActiveIsTrue();

    List<Product> findAllByFeaturedIsTrue();
}
