package com.computerstore.repository;

import com.computerstore.model.products.Motherboard;

public interface MotherboardRepository extends ProductBaseRepository<Motherboard> {
}