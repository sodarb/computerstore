package com.computerstore.repository;

import com.computerstore.model.products.Processor;

public interface ProcessorRepository extends ProductBaseRepository<Processor> {
}
