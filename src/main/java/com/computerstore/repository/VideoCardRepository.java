package com.computerstore.repository;

import com.computerstore.model.products.VideoCard;

public interface VideoCardRepository extends ProductBaseRepository<VideoCard> {
}
