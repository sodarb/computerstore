package com.computerstore.repository;

import com.computerstore.model.User;
import com.computerstore.model.products.Motherboard;
import com.computerstore.model.products.Processor;
import com.computerstore.model.products.VideoCard;
import com.computerstore.model.security.Role;
import com.computerstore.model.security.UserRole;
import com.computerstore.service.UserService;
import com.computerstore.service.products.ProductService;
import com.computerstore.utility.SecurityUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class TestDataPopulator {

    //products
    private static final String[] PRODUCT_NAME = new String[]{
            "AMD RYZEN 5 1600 6-Core Desktop Processor",
            "AMD RYZEN Threadripper 1950X 16-Core Desktop Processor",
            "Intel Core i7-7700K Kaby Lake Quad-Core Desktop Processor",
            "GIGABYTE G1 Gaming GA-Z170X-Gaming 7 Motherboard",
            "GIGABYTE GA-AB350-GAMING 3 Motherboard",
            "GIGABYTE X399 AORUS Gaming 7 Motherboard",
            "MSI GeForce GTX 1060 GAMING 6G",
            "EVGA GeForce GTX 1080 Ti FTW3 GAMING",
            "MSI Black Radeon RX Vega 64"
    };

    private static final String[] PRODUCT_BRAND = new String[]{
            "AMD",
            "AMD",
            "Intel",
            "Gigabyte",
            "Gigabyte",
            "Gigabyte",
            "MSI",
            "EVGA",
            "MSI"
    };

    private static final String[] PRODUCT_MODEL = new String[]{
            "YD1600BBAEBOX",
            "YD195XA8AEWOF",
            "BX80677I77700K",
            "GA-Z170X-Gaming 7",
            "GA-AB350-GAMING 3",
            "X399 AORUS Gaming 7",
            "GeForce GTX 1060 6GB",
            "GeForce GTX 1080 Ti",
            "Radeon RX Vega 64"
    };

    private static final String[] PRODUCT_LIST_PRICE = new String[]{
            "220",
            "1050",
            "350",
            "240",
            "110",
            "400",
            "320",
            "810",
            "800"
    };

    private static final String[] PRODUCT_OUR_PRICE = new String[]{
            "215",
            "1015",
            "340",
            "140",
            "100",
            "390",
            "300",
            "800",
            "720"
    };
    private static final String[] PRODUCT_FEATURED = new String[]{
            "true",
            "true",
            "false",
            "true",
            "false",
            "false",
            "true",
            "false",
            "true"
    };
    private static final String[] PRODUCT_STOCK = new String[]{
            "10",
            "5",
            "9",
            "11",
            "4",
            "3",
            "10",
            "7",
            "3"
    };


    //Processors
    private static final int NUMBER_OF_PROCESSORS = 3;
    private static final String[] PROCESSOR_SOCKET_TYPES = new String[]{
            "AM4",
            "AM4",
            "LGA 1151"
    };

    private static final String[] PROCESSOR_NUMBER_OF_CORES = new String[]{
            "6",
            "16",
            "4"
    };

    private static final String[] PROCESSOR_NUMBER_OF_THREADS = new String[]{
            "12",
            "32",
            "8"
    };

    private static final String[] PROCESSOR_BASE_FREQUENCY = new String[]{
            "3.2",
            "3.4",
            "4.2"
    };

    private static final String[] PROCESSOR_TURBO_FREQUENCY = new String[]{
            "3.6",
            "4",
            "4.5"
    };

    private static final String[] PROCESSOR_TDP = new String[]{
            "65",
            "180",
            "91"
    };


    //Motherboards
    private static final int NUMBER_OF_MOTHERBOARDS = 3;
    private static final String[] MOTHERBOARD_CPU_SOCKET_TYPE = new String[]{
            "LGA 1151",
            "AM4",
            "sTR4"

    };

    private static final String[] MOTHERBOARD_CHIPSET = new String[]{
            "Z170",
            "B350",
            "X399"
    };

    private static final String[] MOTHERBOARD_MAX_MEMORY = new String[]{
            "64",
            "64",
            "128"
    };

    private static final String[] MOTHERBOARD_CHANNEL = new String[]{
            "DUAL",
            "DUAL",
            "QUAD"
    };

    private static final String[] MOTHERBOARD_AUDIO = new String[]{
            "Creative Sound Core 3D chip",
            "Realtek ALC1220",
            "Realtek ALC1220"
    };

    private static final String[] MOTHERBOARD_FROM_FACTOR = new String[]{
            "ATX",
            "ATX",
            "EATX"
    };

    //Video Cards

    private static final int NUMBER_OF_VIDEO_CARDS = 3;
    private static final String[] VIDEO_CARD_CHIPSET_MANUFACTURER = new String[]{
            "Nvidia",
            "Nvidia",
            "AMD"

    };

    private static final String[] VIDEO_CARD_CORE_CLOCK = new String[]{
            "1531",
            "1569",
            "1247"
    };

    private static final String[] VIDEO_CARD_BOOST_CLOCK = new String[]{
            "1746",
            "1683",
            "1546"
    };

    private static final String[] VIDEO_CARD_MEMORY = new String[]{
            "6",
            "11",
            "8"
    };
    private static final String[] VIDEO_CARD_MEMORY_CLOCK = new String[]{
            "8000",
            "11000",
            "1000"
    };


    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    public void createAdmin() throws Exception {
        if (userService.findByUsername("admin") == null) {
            User admin = new User();
            admin.setFirstName("admin");
            admin.setLastName("admin");
            admin.setPhoneNumber("1111111");
            admin.setUsername("admin");
            admin.setEmail("admin@email.com");
            admin.setPassword(SecurityUtility.passwordEncoder().encode("password"));
            Set<UserRole> userRoles = new HashSet<>();
            Role role = new Role();
            role.setName("ROLE_ADMIN");
            role.setRoleId(2);
            userRoles.add(new UserRole(admin, role));

            userService.createUser(admin, userRoles);

        }
    }

    private void createProcessors() {
        for (int i = 0; i < NUMBER_OF_PROCESSORS; i++) {
            if(productService.findById(new Long(i+1)) == null){
                Processor processor = new Processor();
                processor.setId(new Long(i+1));
                processor.setName(PRODUCT_NAME[i]);
                processor.setBrand(PRODUCT_BRAND[i]);
                processor.setModel(PRODUCT_MODEL[i]);
                processor.setListPrice(Double.parseDouble(PRODUCT_LIST_PRICE[i]));
                processor.setOurPrice(Double.parseDouble(PRODUCT_OUR_PRICE[i]));
                processor.setActive(true);
                processor.setFeatured(Boolean.parseBoolean(PRODUCT_FEATURED[i]));
                processor.setStock(Integer.parseInt(PRODUCT_STOCK[i]));
                processor.setSocketType(PROCESSOR_SOCKET_TYPES[i]);
                processor.setNumberOfCores(Integer.parseInt(PROCESSOR_NUMBER_OF_CORES[i]));
                processor.setNumberOfThreads(Integer.parseInt(PROCESSOR_NUMBER_OF_THREADS[i]));
                processor.setBaseFrequency(Double.parseDouble(PROCESSOR_BASE_FREQUENCY[i]));
                processor.setTurboFrequency(Double.parseDouble(PROCESSOR_TURBO_FREQUENCY[i]));
                processor.setTDP(Double.parseDouble(PROCESSOR_TDP[i]));

                productService.save(processor);
            }

        }
    }

    private void createMotherboards() {
        for (int i = NUMBER_OF_PROCESSORS; i < NUMBER_OF_PROCESSORS + NUMBER_OF_MOTHERBOARDS; i++) {
            int j = 0;
            if(productService.findById(new Long(i+1)) == null){
                Motherboard motherboard = new Motherboard();
                motherboard.setId(new Long(i+1));
                motherboard.setName(PRODUCT_NAME[i]);
                motherboard.setBrand(PRODUCT_BRAND[i]);
                motherboard.setModel(PRODUCT_MODEL[i]);
                motherboard.setListPrice(Double.parseDouble(PRODUCT_LIST_PRICE[i]));
                motherboard.setOurPrice(Double.parseDouble(PRODUCT_OUR_PRICE[i]));
                motherboard.setActive(true);
                motherboard.setFeatured(Boolean.parseBoolean(PRODUCT_FEATURED[i]));
                motherboard.setStock(Integer.parseInt(PRODUCT_STOCK[i]));
                motherboard.setAudioChipset(MOTHERBOARD_AUDIO[j]);
                motherboard.setChannelSupported(MOTHERBOARD_CHANNEL[j]);
                motherboard.setChipset(MOTHERBOARD_CHIPSET[j]);
                motherboard.setCPUSocketType(MOTHERBOARD_CPU_SOCKET_TYPE[j]);
                motherboard.setFormFactor(MOTHERBOARD_FROM_FACTOR[j]);
                motherboard.setMaximumSupportedMemoryInGB(Integer.parseInt(MOTHERBOARD_MAX_MEMORY[j]));

                productService.save(motherboard);
            }

        }

    }

    private void createVideoCards() {
        for (int i = NUMBER_OF_PROCESSORS + NUMBER_OF_MOTHERBOARDS; i < NUMBER_OF_PROCESSORS + NUMBER_OF_MOTHERBOARDS + NUMBER_OF_VIDEO_CARDS; i++) {
            int j= 0;
            if(productService.findById(new Long(i+1)) == null){
                VideoCard videoCard = new VideoCard();
                videoCard.setId(new Long(i+1));
                videoCard.setName(PRODUCT_NAME[i]);
                videoCard.setBrand(PRODUCT_BRAND[i]);
                videoCard.setModel(PRODUCT_MODEL[i]);
                videoCard.setListPrice(Double.parseDouble(PRODUCT_LIST_PRICE[i]));
                videoCard.setOurPrice(Double.parseDouble(PRODUCT_OUR_PRICE[i]));
                videoCard.setActive(true);
                videoCard.setFeatured(Boolean.parseBoolean(PRODUCT_FEATURED[i]));
                videoCard.setStock(Integer.parseInt(PRODUCT_STOCK[i]));
                videoCard.setBoostClock(Integer.parseInt(VIDEO_CARD_BOOST_CLOCK[j]));
                videoCard.setChipsetManufacturer(VIDEO_CARD_CHIPSET_MANUFACTURER[j]);
                videoCard.setCoreClock(Integer.parseInt(VIDEO_CARD_CORE_CLOCK[j]));
                videoCard.setMemoryClock(Integer.parseInt(VIDEO_CARD_MEMORY_CLOCK[j]));
                videoCard.setMemoryInGb(Integer.parseInt(VIDEO_CARD_MEMORY[j]));

                productService.save(videoCard);
            }

        }
    }

    public void createProducts(){
        createProcessors();
        createMotherboards();
        createVideoCards();
    }
}
