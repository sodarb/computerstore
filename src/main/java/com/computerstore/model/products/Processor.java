package com.computerstore.model.products;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Entity;

@Entity
public class Processor extends Product {
    private String socketType;
    private Integer numberOfCores;
    private Integer numberOfThreads;
    private Double baseFrequency;
    private Double turboFrequency;
    private Double TDP;

    public Processor(){}

    public Processor(String brand, String name, String model, Double listPrice, Double ourPrice, boolean featured, boolean active, Integer stock, MultipartFile image, String socketType, Integer numberOfCores, Integer numberOfThreads, Double baseFrequency, Double turboFrequency, Double TDP) {
        super(brand, name, model, listPrice, ourPrice, featured, active, stock, image);
        this.socketType = socketType;
        this.numberOfCores = numberOfCores;
        this.numberOfThreads = numberOfThreads;
        this.baseFrequency = baseFrequency;
        this.turboFrequency = turboFrequency;
        this.TDP = TDP;
    }

    public String getSocketType() {
        return socketType;
    }

    public void setSocketType(String socketType) {
        this.socketType = socketType;
    }

    public Integer getNumberOfCores() {
        return numberOfCores;
    }

    public void setNumberOfCores(Integer numberOfCores) {
        this.numberOfCores = numberOfCores;
    }

    public Integer getNumberOfThreads() {
        return numberOfThreads;
    }

    public void setNumberOfThreads(Integer numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public Double getBaseFrequency() {
        return baseFrequency;
    }

    public void setBaseFrequency(Double baseFrequency) {
        this.baseFrequency = baseFrequency;
    }

    public Double getTurboFrequency() {
        return turboFrequency;
    }

    public void setTurboFrequency(Double turboFrequency) {
        this.turboFrequency = turboFrequency;
    }

    public Double getTDP() {
        return TDP;
    }

    public void setTDP(Double TDP) {
        this.TDP = TDP;
    }
}
