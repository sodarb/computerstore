package com.computerstore.model.products;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Entity;

@Entity
public class VideoCard extends Product {
    private String chipsetManufacturer;
    private Integer coreClock;
    private Integer boostClock;
    private Integer memoryInGb;
    private Integer memoryClock;

    public VideoCard() {
    }

    public VideoCard(String brand, String name, String model, Double listPrice, Double ourPrice, boolean featured, boolean active, Integer stock, MultipartFile image, String chipsetManufacturer, Integer coreClock, Integer boostClock, Integer memoryInGb, Integer memoryClock) {
        super(brand, name, model, listPrice, ourPrice, featured, active, stock, image);
        this.chipsetManufacturer = chipsetManufacturer;
        this.coreClock = coreClock;
        this.boostClock = boostClock;
        this.memoryInGb = memoryInGb;
        this.memoryClock = memoryClock;
    }

    public String getChipsetManufacturer() {
        return chipsetManufacturer;
    }

    public void setChipsetManufacturer(String chipsetManufacturer) {
        this.chipsetManufacturer = chipsetManufacturer;
    }

    public Integer getCoreClock() {
        return coreClock;
    }

    public void setCoreClock(Integer coreClock) {
        this.coreClock = coreClock;
    }

    public Integer getBoostClock() {
        return boostClock;
    }

    public void setBoostClock(Integer boostClock) {
        this.boostClock = boostClock;
    }

    public Integer getMemoryInGb() {
        return memoryInGb;
    }

    public void setMemoryInGb(Integer memoryInGb) {
        this.memoryInGb = memoryInGb;
    }

    public Integer getMemoryClock() {
        return memoryClock;
    }

    public void setMemoryClock(Integer memoryClock) {
        this.memoryClock = memoryClock;
    }
}
