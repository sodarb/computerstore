package com.computerstore.model.products;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Entity;

@Entity
public class Motherboard extends Product {
    private String CPUSocketType;
    private String chipset;
    private Integer maximumSupportedMemoryInGB;
    private String channelSupported;
    private String audioChipset;
    private String formFactor;



    public Motherboard(){}

    public Motherboard(String brand, String name, String model, Double listPrice, Double ourPrice, boolean featured, boolean active, Integer stock, MultipartFile image, String CPUSocketType, String chipset, Integer maximumSupportedMemoryInGB, String channelSupported, String audioChipset, String formFactor) {
        super(brand, name, model, listPrice, ourPrice, featured, active, stock, image);
        this.CPUSocketType = CPUSocketType;
        this.chipset = chipset;
        this.maximumSupportedMemoryInGB = maximumSupportedMemoryInGB;
        this.channelSupported = channelSupported;
        this.audioChipset = audioChipset;
        this.formFactor = formFactor;
    }

    public String getCPUSocketType() {
        return CPUSocketType;
    }

    public void setCPUSocketType(String CPUSocketType) {
        this.CPUSocketType = CPUSocketType;
    }

    public String getChipset() {
        return chipset;
    }

    public void setChipset(String chipset) {
        this.chipset = chipset;
    }

    public Integer getMaximumSupportedMemoryInGB() {
        return maximumSupportedMemoryInGB;
    }

    public void setMaximumSupportedMemoryInGB(Integer maximumSupportedMemoryInGB) {
        this.maximumSupportedMemoryInGB = maximumSupportedMemoryInGB;
    }

    public String getChannelSupported() {
        return channelSupported;
    }

    public void setChannelSupported(String channelSupported) {
        this.channelSupported = channelSupported;
    }

    public String getAudioChipset() {
        return audioChipset;
    }

    public void setAudioChipset(String audioChipset) {
        this.audioChipset = audioChipset;
    }

    public String getFormFactor() {
        return formFactor;
    }

    public void setFormFactor(String formFactor) {
        this.formFactor = formFactor;
    }
}

