package com.computerstore.model.products;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Product {

    @Id
    @GeneratedValue(strategy =GenerationType.AUTO)
    @Column(name = "id",updatable = false, nullable = false)
    private Long id;
    private String brand;
    private String name;
    private String model;
    private Double listPrice;
    private Double ourPrice;
    private boolean featured;
    private boolean active;
    private Integer stock;

    @Transient
    private MultipartFile image;

    public Product(){}


    public Product(String brand, String name, String model, Double listPrice, Double ourPrice, boolean featured, boolean active, Integer stock, MultipartFile image) {
        this.brand = brand;
        this.name = name;
        this.model = model;
        this.listPrice = listPrice;
        this.ourPrice = ourPrice;
        this.featured = featured;
        this.active = active;
        this.stock = stock;
        this.image = image;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getListPrice() {
        return listPrice;
    }

    public void setListPrice(Double listPrice) {
        this.listPrice = listPrice;
    }

    public Double getOurPrice() {
        return ourPrice;
    }

    public void setOurPrice(Double ourPrice) {
        this.ourPrice = ourPrice;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }
}
