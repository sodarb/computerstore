var slideIndex = 1;
showSlides(slideIndex);
showSlidesAuto();

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n){
    showSlides(slideIndex = n);
}

function showSlides(n){
    var i;
    var slides = document.getElementsByClassName("slide");
    var dots = document.getElementsByClassName("dot");
    if(n > slides.length){
        slideIndex = 1;
    }
    if(n < 1){
        slideIndex = slides.length;
    }

    for( i= 0; i <slides.length; i++){
        slides[i].style.display = "none";
    }

    for(i=0; i<dots.length; i++){
        dots[i].className = dots[i].className.replace(" active", "");
    }

    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className+= " active";
}

function showSlidesAuto() {
    var i;
    var slides=document.getElementsByClassName("slide");
    var dots = document.getElementsByClassName("dot");
    for(i=0;i<slides.length; i++){
        slides[i].style.display = "none";
    }
    for(i=0;i<slides.length;i++){
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slideIndex++;
    if(slideIndex>slides.length){
        slideIndex =1;
    }
    slides[slideIndex-1].style.display="block";
    dots[slideIndex-1].className+= " active";
    setTimeout(showSlidesAuto,10000);
}


var featuredSlideIndex = 1;
showSlidesAuto();
function showFeaturedSlidesAuto() {
    var i;
    var slides=document.getElementsByClassName("featuredSlide");
    for(i=0;i<slides.length; i++){
        slides[i].style.display = "none";
    }
    featuredSlideIndex++;
    if(slideIndex>slides.length){
        slideIndex =1;
    }
    slides[slideIndex-1].style.display="block";
    setTimeout(showSlidesAuto,10000);
}