
function enableEdit() {
    $(".editInput").prop('disabled',false);
    $(".cancelbtn").prop('hidden',false);
}

function disableEdit(){
    $(".editInput").prop('disabled',true);
    $(".cancelbtn").prop('hidden',true);
}